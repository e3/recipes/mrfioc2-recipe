#!/bin/bash


LIBVERSION=${PKG_VERSION}

# This is the directory that the ESS-specific scripts are extracted into
export ESS_MRFIOC2_DIR=e3wrap
echo "INFO: Build prefix"
echo "${BUILD_PREFIX}"
echo "${EPICS_MODULES}"
echo "${LIBVERSION}"
echo "${PRJ}"

# Use the same Makefile from wrapper
install -m 644 ${ESS_MRFIOC2_DIR}/*.Makefile  ./

# Clean between variants builds
make -f ${ESS_MRFIOC2_DIR}/mrfioc2.Makefile clean
echo "INFO: Clean returns $?"

# copy over ess specific substitutions
# not sure why this copy is needed in order to build the db's from wrapper
install -m 644 ${ESS_MRFIOC2_DIR}/db/*.db  evrMrmApp/Db/
install -m 644 ${ESS_MRFIOC2_DIR}/template/*/*.substitutions  evrMrmApp/Db/
install -m 644 ${ESS_MRFIOC2_DIR}/template/*/*.template  evrMrmApp/Db/

make -f ${ESS_MRFIOC2_DIR}/mrfioc2.Makefile MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} E3_MODULE_VERSION=${LIBVERSION} prebuild
echo "INFO: Prebuild returns $?"

make -f ${ESS_MRFIOC2_DIR}/mrfioc2.Makefile MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} E3_MODULE_VERSION=${LIBVERSION}
echo "INFO: Build returns $?"

make -f ${ESS_MRFIOC2_DIR}/mrfioc2.Makefile MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} E3_MODULE_VERSION=${LIBVERSION} install
echo "INFO: Install returns $?"

# install the ess iocsh scripts into the directory
install -m 644 ${ESS_MRFIOC2_DIR}/iocsh/*/*sh  ${PREFIX}/modules/${PKG_NAME}/${PKG_VERSION}/
install -m 644 ${ESS_MRFIOC2_DIR}/iocsh/*sh  ${PREFIX}/modules/${PKG_NAME}/${PKG_VERSION}/
